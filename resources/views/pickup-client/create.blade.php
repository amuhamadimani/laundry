@php
    $date = date('Y-m-d');    
    $jam = date('H:i:s');    
@endphp

@extends('templates.stisla')

@section('title', 'Laundry')

@section('header')
	<div class="section-header">
		<h1>Pickup Service - Dashboard</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
		</div>
	</div>
@endsection
    
    
@section('sidebar-menu')
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li><a class="nav-link" href="{{ url('pickup-client') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Dashboard"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
    <li class="menu-header">List</li>
    <li><a class="nav-link" href="{{ route('pickup.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Pickup Client"><i class="fas fa-file"></i><span>Pickup Client</span></a></li>
    <li><a class="nav-link" href="{{ route('delivery.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Delivery Client"><i class="fas fa-file"></i><span>Delivery Client</span></a></li>
  </ul>
@endsection

@section('content')
<form action="{{ route('pickup.client.create') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Rumah Sakit</label>
                            <div class="col-md-7">
                                <select name="rumah_sakit" id="get-item" class="form-control" required>
                                    @foreach ($kontrak as $val)
                                        <option value=""{{ $val->no_kontrak }}>{{ getKontrak($val->no_kontrak)->client }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>    
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Lantai</label>
                            <div class="col-md-7">
                                <input type="text" name="lantai" class="form-control" required="" value="" >
                            </div>
                        </div>  
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Ruang Perawatan</label>
                            <div class="col-md-7">
                                <input type="text" name="ruang_perawatan" class="form-control" required="" value="" >
                            </div>
                        </div>  
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">PIC</label>
                            <div class="col-md-7">
                                <input type="text" name="pic" class="form-control" required="" value="" >
                            </div>
                        </div>   
                            
                    </div>
                
                    <div class="col-md-6">
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tanggal</label>
                            <div class="col-md-7">
                                <input type="text" name="tanggal" class="form-control" required="" value="{{$date}}" readonly>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div>
                    <hr>
                </div>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2">
                            <h6 class="text-center">Item Code</h6>
                            <select name="item_code[0]" id="get-item" class="form-control">
                                <option value="">Pilih Item</option>
                                @foreach ($kontrak as $val)
                                    @php
                                        $kode = json_decode($val->kode_item);
                                    @endphp
                                    @foreach ($kode as $val)
                                        <option value="">{{ $val }}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <h6 class="text-center">Item Name</h6>
                            <select name="item_name[0]" id="get-item" class="form-control">
                                <option value="">Pilih Item</option>
                                @foreach ($kontrak as $val)
                                    @php
                                        $nama = json_decode($val->nama_item);
                                    @endphp
                                    @foreach ($nama as $val)
                                        <option value="">{{ $val }}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <h6 class="text-center">Qty</h6>
                            <input type="number" class="form-control" id="get-item" name="qty[0]" id="myorders">
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-success" id="add-item"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                    {{-- append ajax --}}
                    <div id="input-list"> </div>
                    {{-- <input type="hidden" id="key" value="0"> --}}
                    <select name="" type="hidden" id="key">
                        <option value="0"></option>
                    </select>
                    <br>

                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pickup Boy</label>
                            <div class="col-md-7">
                                <input type="text" name="pickup_boy" class="form-control" required="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jam</label>
                            <div class="col-md-7">
                            <input type="text" name="jam" class="form-control" value="{{ $jam }}" required="">
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('pickup.client.create') }}">
                        <div class="col-md-1">
                            <button class="btn btn-success btn-lg">Submit</button>
                        </div>
                    </form>
                    
                </div>
                
        </div>
        </div>
    </div>
</form>
@endsection

@section('script')
  
    <script>
        $(document).ready(function(){

            var key_input = $("#key").val();
            $("#key").hide();

            $("#add-item").on("click", function(e){ 

                e.preventDefault();

                key_input++;

                var url = '/ajax/get-item-client'

                var data = {
                    key:key_input
                }
                
                $.ajax({
                    url:url,
                    data:data,
                    type:"POST",
                    success:function(result){
                        $("#input-list").append(result.html);
                        $("#key").val(result.key);
                    }
                })
            
            });
        });

        $.fn.removeItem = function(id){
            $(id).remove();
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
    </script>

@endsection
