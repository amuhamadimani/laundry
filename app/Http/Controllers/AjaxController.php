<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Kontrak;

class AjaxController extends Controller
{
    public function getItem(Request $request){

        $key = $request->key;

        return response()->json([
            'html' => view('admin-laundry.kontrak.addItem',['key'=>$key])->render(),
            'key' => $key
        ]);
    }

    
    public function getItemClient(Request $request){

        $key = $request->key;
        $kontrak = Kontrak::all();

        return response()->json([
            'html' => view('pickup-client.addItem',['key'=>$key, 'kontrak' => $kontrak])->render(),
            'key' => $key,
            'kontrak' => $kontrak
        ]);
    }
}
