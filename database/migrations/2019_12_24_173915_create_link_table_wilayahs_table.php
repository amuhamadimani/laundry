<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkTableWilayahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_table_wilayah', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('provinsi_id');
            $table->integer('kecamtan_id');
            $table->integer('kabupaten_id');
            $table->integer('kelurahan_id');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_table_wilayahs');
    }
}
