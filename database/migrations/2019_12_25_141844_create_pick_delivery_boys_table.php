<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePickDeliveryBoysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pick_delivery_boy', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->mediumInteger('hp');
            $table->string('sex');
            $table->text('alamat');
            $table->string('pekerjaan');
            $table->string('agama');
            $table->date('tgl_lahir');
            $table->string('suku');
            $table->string('provinsi');
            $table->string('kabupaten');
            $table->string('kecamatan');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pick_delivery_boy');
    }
}
