<?php

namespace App\Http\Controllers;

use App\Model\PicRuanganDetail;
use Illuminate\Http\Request;

class PicRuanganDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\PicRuanganDetail  $picRuanganDetail
     * @return \Illuminate\Http\Response
     */
    public function show(PicRuanganDetail $picRuanganDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\PicRuanganDetail  $picRuanganDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(PicRuanganDetail $picRuanganDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\PicRuanganDetail  $picRuanganDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PicRuanganDetail $picRuanganDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\PicRuanganDetail  $picRuanganDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(PicRuanganDetail $picRuanganDetail)
    {
        //
    }
}
