<?php

namespace App\Http\Controllers;

use App\Model\ClientAccounting;
use Illuminate\Http\Request;

class ClientAccountingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\ClientAccounting  $clientAccounting
     * @return \Illuminate\Http\Response
     */
    public function show(ClientAccounting $clientAccounting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\ClientAccounting  $clientAccounting
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientAccounting $clientAccounting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\ClientAccounting  $clientAccounting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientAccounting $clientAccounting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\ClientAccounting  $clientAccounting
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientAccounting $clientAccounting)
    {
        //
    }
}
