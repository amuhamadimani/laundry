@php
    $date = date('Y-m-d');    
    $jam = date('H:i:s');    
@endphp

@extends('templates.stisla')

@section('title', 'Laundry')

@section('header')
	<div class="section-header">
		<h1>Kontrak - Dashboard</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ route('admin.laundry.index') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ route('admin.laundry.kontrak.index') }}">Kontrak</a>
			</div>
			<div class="breadcrumb-item active">
				<a>Edit</a>
			</div>
		</div>
	</div>
@endsection
    
@section('sidebar-menu')
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li><a class="nav-link" href="{{ url('/admin') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Dashboard"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
    <li class="menu-header">List</li>
    <li><a class="nav-link" href="{{ route('admin.laundry.pickup.boy.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Pickup & Delivery Boy"><i class="fas fa-user"></i><span>Pickup & Delivery Boy</span></a></li>
    <li><a class="nav-link" href="{{ route('admin.laundry.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Maintenance Client"><i class="fas fa-user"></i><span>Maintenance Client</span></a></li>
    <li><a class="nav-link" href="{{ route('admin.laundry.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Maintenance Kontrak"><i class="fas fa-file"></i><span>Maintenance Kontrak</span></a></li>
    <li><a class="nav-link" href="{{ route('users') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Management User"><i class="fas fa-users"></i><span>Management User</span></a></li>
   </ul>
@endsection

@section('content')
<form action="{{ route('admin.laundry.kontrak.store') }}" method="POST"> 
    @csrf
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No Kontrak</label>
                        <div class="col-md-7">
                            <input type="text" name="no_kontrak" class="form-control" required="" readonly value="{{ $edit->no_kontrak }}" >
                        </div>
                    </div>  
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Client</label>
                        <div class="col-md-7">
                            <input type="text" name="client" class="form-control" required="" readonly value="{{ $edit->client }}" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row mb-6">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Periode Kontrak Dari Tanggal</label>
                                <div class="col-md-7">
                                    <input type="date" name="periode_kontrak_mulai" class="form-control" required="" readonly value="{{ $edit->periode_kontrak_mulai }}" >
                                </div>
                            </div>  
                        </div> 
                        <div class="col-md-6">
                            <div class="form-group row mb-6">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Sampai Tanggal</label>
                                <div class="col-md-7">
                                    <input type="date" name="periode_kontrak_akhir" class="form-control" required="" readonly value="{{ $edit->periode_kontrak_akhir }}" >
                                </div>
                            </div>  
                        </div>    
                    </div>  
                     
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jenis Kegiatan</label>
                        <div class="col-md-7">
                            <input type="text" name="jenis_kegiatan" class="form-control" required="" readonly value="{{ $edit->jenis_kegiatan }}" >
                        </div>
                    </div>  
                    
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-5">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kuota Minimal / Bln</label>
                                <div class="col-md-4">
                                <input type="number" name="kuota_minimal" class="form-control" required="" readonly value="{{ $edit->kuota_minimal }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group row mb-2">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Berat Minimal / Bln</label>
                                <div class="col-md-4">
                                <input type="number" name="berat_minimal" class="form-control" required="" readonly value="{{ $edit->berat_minimal }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Periode Tagihan</label>
                                <div class="col-md-7">
                                    <input type="text" name="periode_tagihan" class="form-control" required="" readonly value="{{ $edit->periode_tagihan }}" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Terbit Invoice</label>
                                <div class="col-md-7">
                                <input type="text" name="terbit_invoice" class="form-control" required="" readonly value="{{ $edit->terbit_invoice }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jatuh Tempo</label>
                                <div class="col-md-7">
                                    <input type="date" name="jatuh_tempo" class="form-control" required="" readonly value="{{ $edit->jatuh_tempo }}" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Blokir Pickup</label>
                                <div class="col-md-7">
                                <input type="text" name="blokir_pickup" class="form-control" required="" readonly value="{{ $edit->blokir_pickup }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="card-title">
                        <h5>Item yang Disepakati Dalam Kontrak Ini</h5>
                    </div>
                    <hr>
                    
                    @foreach ($editItem as $item)
                        @php
                            $kode = json_decode($item->kode_item);
                            $nama = json_decode($item->nama_item);
                            $berat = json_decode($item->berat);
                            $harga = json_decode($item->harga);
                            // dd($kode)
                        @endphp
                    <div class="row">
                        <div class="col-md-2">
                            <h6 class="text-center">Kode Item</h6> 
                            @foreach ($kode as $val)
                                <input type="text" class="form-control" readonly name="kode_item[0]" value="{{ $val }}"><br>
                            @endforeach
                        </div>
                        <div class="col-md-4">
                            <h6 class="text-center">Nama Item</h6>
                                @foreach ($nama as $val)
                                    <input type="text" class="form-control" readonly name="nama_item[0]" value="{{ $val }}"><br>
                                @endforeach
                        </div>
                        <div class="col-md-2">
                            <h6 class="text-center">Berat (gr)</h6>
                                @foreach ($berat as $val)
                                    <input type="number" class="form-control" readonly name="berat[0]" value="{{ $val }}"><br>
                                @endforeach
                        </div>
                        <div class="col-md-2">
                            <h6 class="text-center">Harga /gr</h6>
                                @foreach ($harga as $val)
                                    <input type="number" class="form-control" readonly name="harga[0]" value="{{ $val }}"> <br>
                                @endforeach
                        </div>

                        {{-- <div class="col-md-1">
                            <button type="button" class="btn btn-success" id="add-order"><i class="fa fa-plus"></i></button>
                        </div> --}}
                    </div>
                    @endforeach

                    {{-- append ajax --}}
                    <div id="input-list"> </div>
                    <input type="hidden" id="key" value="0">
                    <br>
                
                    <div class="card-footer text-right">
                        <a href="{{ url('admin-laundry/kontrak/delete/'.$edit->id) }}" class="btn btn-danger btn-lg">Hapus</a>
                    </div>
                </form>
                    
                </div>
      </div>
    </div>
</div>
@endsection
{{-- <table class="table table-bordered table-striped table-hover" id="dabel">

    <thead>
        <tr>
            <td>No</td>
            <td>Kode Item</td>
            <td>Nama Item</td>
            <td>Berat (gr)</td>
            <td>Harga/gr</td>
        </tr>
    </thead>
    @foreach ($editItem as $item)
        @php
            $kode = json_decode($item->kode_item);
        @endphp
       <tr>
           <td>{{ $loop->index+1 }}</td>
           @foreach ($kode as $val)
               <td>{{ $val }}</td>
           @endforeach
            <td></td>
            <td></td>
            <td></td>   
        </tr> 
    @endforeach
</table> --}}
@section('script')
  
    <script>
        $(document).ready(function(){

            var key_input = $("#key").val();

            $("#add-order").on("click", function(e){ 

                e.preventDefault();

                key_input++;

                var url = '/ajax/get-item'

                var data = {
                    key:key_input
                }
                
                $.ajax({
                    url:url,
                    data:data,
                    type:"POST",
                    success:function(result){
                        $("#input-list").append(result.html);
                        $("#key").val(result.key);
                    }
                })
            
            });
        });

        $.fn.removeItem = function(id){
            $(id).remove();
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
    </script>

@endsection


