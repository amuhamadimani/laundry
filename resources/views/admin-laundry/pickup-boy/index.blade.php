@extends('templates.stisla')

@section('title', 'Laundry')

@section('header')
	<div class="section-header">
		<h1>Pickup Deliver - Dashboard</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item active">
				<a>Pickup Boy</a>
			</div>
		</div>
	</div>
@endsection

@section('sidebar-menu')
<ul class="sidebar-menu">
  <li class="menu-header">Dashboard</li>
  <li><a class="nav-link" href="{{ url('/admin') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Dashboard"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
  <li class="menu-header">List</li>
  <li><a class="nav-link" href="{{ route('admin.laundry.pickup.boy.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Pickup & Delivery Boy"><i class="fas fa-user"></i><span>Pickup & Delivery Boy</span></a></li>
  <li><a class="nav-link" href="{{ route('admin.laundry.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Maintenance Client"><i class="fas fa-user"></i><span>Maintenance Client</span></a></li>
  <li><a class="nav-link" href="{{ route('admin.laundry.kontrak.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Maintenance Kontrak"><i class="fas fa-file"></i><span>Maintenance Kontrak</span></a></li>
  <li><a class="nav-link" href="{{ route('users') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Management User"><i class="fas fa-users"></i><span>Management User</span></a></li>
</ul>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">  
              <table class="table table-bordered table-striped table-hover" id="dabel">
                <a href="{{ route('admin.laundry.pickup.boy.create') }}" class="a btn btn-info btn-lg float-right"><i class="fa fa-plus"></i></a>
                <br>
                <h4 class="card-title">Maintenance Pickup-Delivery Boy</h4>

                  <thead class="text-center">
                      <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>No Hp</th>
                          <th>Sex</th>
                          <th>Alamat</th>
                          <th>Pekerjaan</th>
                          <th>Agama</th>
                          <th>Tgl Lahir</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($data as $val)
                          <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ $val->nama }}</td>
                            <td>{{ $val->hp }}</td>
                            <td>{{ $val->sex }}</td>
                            <td>{{ $val->alamat }}</td>
                            <td>{{ $val->pekerjaan }}</td>
                            <td>{{ $val->agama }}</td>
                            <td>{{ $val->tgl_lahir }}</td>
                            <td>
                              <a href="{{ url('admin-laundry/pickup-boy/edit/'.$val->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                              <a href="{{ url('admin-laundry/pickup-boy/delete/'.$val->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                          </tr>
                      @endforeach
                  </tbody>
              </table>  
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection