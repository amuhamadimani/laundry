<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'client';

    protected $fillable = [
        'no_client',
        'no_kontrak',
        'tanggal',
        'nama',
        'alamat',
        'telepon',
        'pic',
        'provinsi',
        'kabupaten',
        'kecamatan',
        'kode_pos',
    ];

    public function jenisLayanan() {
		return Kontrak::select('jenis_layanan')->where('no_kontrak', $id)->firts();
	}
}
