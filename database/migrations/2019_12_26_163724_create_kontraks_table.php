<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKontraksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kontrak', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_kontrak');
            $table->string('client');
            $table->date('periode_kontrak_mulai');
            $table->date('periode_kontrak_akhir');
            $table->string('jenis_kegiatan');
            $table->integer('kuota_minimal');
            $table->integer('berat_minimal');
            $table->string('periode_tagihan');
            $table->date('jatuh_tempo');
            $table->string('terbit_invoice');
            $table->string('blokir_pickup');
            $table->string('kode_item');
            $table->string('nama_item');
            $table->integer('berat');
            $table->integer('harga');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kontrak');
    }
}
