@php
    $date = date('Y-m-d');
@endphp
@extends('templates.stisla')

@section('title', 'Laundry')

@section('header')
	<div class="section-header">
		<h1>Pickup Service - Transaksi</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Transaksi</a>
			</div>
		</div>
	</div>
@endsection

@section('sidebar-menu')
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li><a class="nav-link" href="{{ url('pickup-boy') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Dashboard"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
    <li class="menu-header">List</li>
    <li><a class="nav-link" href="{{ route('pickup.boy.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Pickup Boy"><i class="fas fa-file"></i><span>Pickup Boy</span></a></li>
    <li><a class="nav-link" href="{{ route('delivery.boy.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Delivery Boy"><i class="fas fa-file"></i><span>Delivery Boy</span></a></li>
  </ul>
@endsection

@section('content')
<form action="{{ url('pickup-delivery-boy/edit/'.$edit->id) }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-body">
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Rumah Sakit</label>
                        <div class="col-md-7">
                            <input type="text" name="nama" class="form-control" required="" value="{{$edit->rumah_sakit}}" readonly>
                        </div>
                    </div>    
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Lantai</label>
                        <div class="col-md-7">
                            <input type="text" name="nama" class="form-control" required="" value="{{$edit->lantai}}" readonly>
                        </div>
                    </div>  
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Ruang Perawatan</label>
                        <div class="col-md-7">
                            <input type="text" name="nama" class="form-control" required="" value="{{$edit->ruang_perawatan}}" readonly>
                        </div>
                    </div>  
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">PIC</label>
                        <div class="col-md-7">
                            <input type="text" name="nama" class="form-control" required="" value="{{$edit->pic}}" readonly>
                        </div>
                    </div>   
                        
                </div>

                <div class="col-md-6">
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tanggal</label>
                        <div class="col-md-7">
                            <input type="text" name="nama" class="form-control" required="" value="{{$date}}" readonly>
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-12">
                    <hr>
                </div>
                
                @foreach ($editItem as $item)
                    @php
                        $kode = json_decode($item->item_code);
                        $nama = json_decode($item->item_name);
                        $qty = json_decode($item->qty);
                        // dd($nama);
                    @endphp
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <h6 class="text-center">Item Code</h6>
                                @foreach ($kode as $val)
                                    <input type="text" class="form-control" value="{{ $val }}" readonly name="item_code[0]"><br>
                                @endforeach
                            </div>
                            <div class="col-md-6">
                                <h6 class="text-center">Item Name</h6>
                                @foreach ($nama as $val)
                                    <input type="text" class="form-control" value="{{ $val }}" readonly name="nama_item[0]"><br>
                                @endforeach
                            </div>
                            <div class="col-md-2">
                                <h6 class="text-center">Qty</h6>
                                @foreach ($qty as $val)
                                    <input type="number" class="form-control" value="{{ $val }}" name="qty[]"><br>
                                @endforeach
                            </div>
                        </div>
                @endforeach
            </div>
            <br>

            <div class="card-footer text-right">
                <button class="btn btn-primary btn-lg">Submit</button>
            </div>
        </div>
        </div>
    </div>
</form>

@endsection