<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 'kelurahan';

    protected $fillable = [
        'kecamatan_id',
        'kabupaten_id',
        'provinsi_id',
        'keluarahan_name',
        'kode_pos'
    ];
}
