<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kontrak extends Model
{
    protected $table = 'kontrak';

    protected $fillable = [
        'no_kontrak',
        'client',
        'periode_kontrak_mulai',
        'periode_kontrak_akhir',
        'jenis_kegiatan',
        'kuota_minimal',
        'berat_minimal',
        'periode_tagihan',
        'jatuh_tempo',
        'terbit_invoice',
        'blokir_pickup',
        'kode_item',
        'nama_item',
        'berat',
        'harga',
    ];

}
