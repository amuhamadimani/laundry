<?php

namespace App\Http\Controllers;

use App\Model\LinkTableWilayah;
use Illuminate\Http\Request;

class LinkTableWilayahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\LinkTableWilayah  $linkTableWilayah
     * @return \Illuminate\Http\Response
     */
    public function show(LinkTableWilayah $linkTableWilayah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\LinkTableWilayah  $linkTableWilayah
     * @return \Illuminate\Http\Response
     */
    public function edit(LinkTableWilayah $linkTableWilayah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\LinkTableWilayah  $linkTableWilayah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LinkTableWilayah $linkTableWilayah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\LinkTableWilayah  $linkTableWilayah
     * @return \Illuminate\Http\Response
     */
    public function destroy(LinkTableWilayah $linkTableWilayah)
    {
        //
    }
}
