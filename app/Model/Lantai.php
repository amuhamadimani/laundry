<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lantai extends Model
{
    protected $table = 'lantai';

    protected $fillable = [
        'lantai_name',
    ];

}
