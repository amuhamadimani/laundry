<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PickupClient extends Model
{
    protected $table = 'pickup_client';
    
    protected $fillable = [
        'rumah_sakit',
        'lantai',
        'ruang_perawatan',
        'pic',
        'tanggal',
        'item_code',
        'item_name',
        'qty',
        'pickup_boy',
        'jam',
    ];
}
