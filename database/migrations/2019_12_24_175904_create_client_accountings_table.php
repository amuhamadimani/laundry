<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientAccountingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_accounting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_acc_id');
            $table->string('client_acc_name');
            $table->integer('client_acc_no_telp');
            $table->string('client_acc_ph1');
            $table->string('client_acc_ph2');
            $table->string('client_acc_email');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_accountings');
    }
}
