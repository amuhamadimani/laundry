<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => 'pickup.delivery.boy'], function(){

    Route::group(['prefix' => 'pickup-boy', 'as' => 'pickup.boy.'], function () {
        Route::get('/', 'PickupDeliveryBoy\PickupBoyController@index')->name('index');
        Route::match(['get', 'post'], 'edit/{id}', 'PickupDeliveryBoy\PickupBoyController@edit')->name('edit');
        Route::post('delete/{id}', 'PickupBoy@delete')->name('delete');
    });
    
    Route::group(['prefix' => 'delivery-boy', 'as' => 'delivery.boy.'], function () {
        Route::get('/', 'PickupDeliveryBoy\DeliveryBoyController@index')->name('index');
        Route::match(['get', 'post'], 'edit/{id}', 'PickupDeliveryBoy\DeliveryBoyController@edit')->name('edit');
        Route::post('delete/{id}', 'PickupBoy@delete')->name('delete');
    });

    Route::group(['prefix' => 'user-management'], function() {
        });
});

Route::group(['middleware' => 'pickup.delivery.client'], function(){

    Route::group(['prefix' => 'pickup-client', 'as' => 'pickup.client.'], function () {
        Route::get('/', 'PickupDeliveryClient\PickupClientController@index')->name('index');
        Route::match(['get', 'post'], 'create', 'PickupDeliveryClient\PickupClientController@create')->name('create');
        Route::match(['get', 'post'], 'edit/{id}', 'PickupDeliveryClient\PickupClientController@edit')->name('edit');
        Route::post('delete/{id}', 'PickupBoy@delete')->name('delete');
    });
    
    Route::group(['prefix' => 'delivery-client', 'as' => 'delivery.client.'], function () {
        Route::get('/', 'PickupDeliveryClient\DeliveryClientController@index')->name('index');
        Route::match(['get', 'post'], 'create', 'PickupDeliveryClient\DeliveryClientController@create')->name('create');
        Route::match(['get', 'post'], 'edit/{id}', 'PickupDeliveryClient\DeliveryClientController@edit')->name('edit');
        Route::post('delete/{id}', 'PickupBoy@delete')->name('delete');
    });
});

Route::group(['middleware' => 'admin', 'prefix' => 'admin-laundry', 'as' => 'admin.laundry.'], function () {
    Route::get('/', 'AdminLaundryController@index')->name('index');

    Route::group(['prefix' => 'pickup-boy', 'as' => 'pickup.boy.'], function(){
        Route::get('/', 'Admin\PickupDeliveryController@index')->name('index');
        Route::get('create', 'Admin\PickupDeliveryController@create')->name('create');
        Route::post('save', 'Admin\PickupDeliveryController@store')->name('store');
        Route::match(['get', 'post'], 'edit/{id}', 'Admin\PickupDeliveryController@edit')->name('edit');
        Route::match(['get', 'post'], 'update/{id}', 'Admin\PickupDeliveryController@update')->name('update');
        Route::get('delete/{id}', 'Admin\PickupDeliveryController@delete')->name('delete');
    });
    
    Route::group(['prefix' => 'client', 'as' => 'client.'], function(){
        Route::get('/', 'Admin\ClientController@index')->name('index');
        Route::get('create', 'Admin\ClientController@create')->name('create');
        Route::post('create/save', 'Admin\ClientController@store')->name('store');
        Route::match(['get', 'post'], 'edit/{id}', 'Admin\ClientController@edit')->name('edit');
        Route::match(['get', 'post'], 'update/{id}', 'Admin\ClientController@update')->name('update');
        Route::post('delete/{id}', 'Client@delete')->name('delete');
    });
    
    Route::group(['prefix' => 'kontrak', 'as' => 'kontrak.'], function(){
        Route::get('/', 'Admin\KontrakController@index')->name('index');
        Route::get('create', 'Admin\KontrakController@create')->name('create');
        Route::post('create/save', 'Admin\KontrakController@store')->name('store');
        Route::match(['get', 'post'], 'edit/{id}', 'Admin\KontrakController@edit')->name('edit');
        Route::match(['get', 'post'], 'update/{id}', 'Admin\KontrakController@update')->name('update');
        Route::get('delete/{id}', 'Admin\KontrakController@delete')->name('delete');
        Route::get('getItem', 'Admin\KontrakController@create')->name('getItem');
    });
    
});

// Route::get('/home', 'PickupBoyController@index')->name('home');

// AJAX
Route::post('/ajax/get-item', 'AjaxController@getItem')->name('getItem');
Route::post('/ajax/get-item-client', 'AjaxController@getItemClient')->name('getItemClient');
Route::post('/ajax/get-item-del-client', 'AjaxController@getItemClient')->name('getItemClient');