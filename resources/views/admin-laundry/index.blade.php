@extends('templates.stisla')

@section('title', 'Laundry')

@section('sidebar-menu')
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li><a class="nav-link" href="{{ url('/admin') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Dashboard"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
    <li class="menu-header">List</li>
    <li><a class="nav-link" href="{{ route('admin.laundry.pickup.boy.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Pickup & Delivery Boy"><i class="fas fa-user"></i><span>Pickup & Delivery Boy</span></a></li>
    <li><a class="nav-link" href="{{ route('admin.laundry.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Maintenance Client"><i class="fas fa-user"></i><span>Maintenance Client</span></a></li>
    <li><a class="nav-link" href="{{ route('users') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Management User"><i class="fas fa-users"></i><span>Management User</span></a></li>
  </ul>
@endsection

@section('header')
	<div class="section-header">
		<h1>Admin - Dashboard</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<div>Dashboard</div>
			</div>
		</div>
	</div>
@endsection
    
@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">   
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection