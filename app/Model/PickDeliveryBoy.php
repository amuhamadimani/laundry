<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PickDeliveryBoy extends Model
{
    protected $table = 'pick_delivery_boy';

    protected $fillable = [
        'nama',
        'hp',
        'sex',
        'alamat',
        'pekerjaan',
        'agama',
        'tgl_lahir',
        'suku',
        'provinsi',
        'kabupaten',
        'kecamatan',
    ];
}
