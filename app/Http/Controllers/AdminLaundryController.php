<?php

namespace App\Http\Controllers;

use App\Model\AdminLaundry;
use Illuminate\Http\Request;

class AdminLaundryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin-laundry.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
        /**
     * Display the specified resource.
     *
     * @param  \App\Model\AdminLaundry  $adminLaundry
     * @return \Illuminate\Http\Response
     */
    public function show(AdminLaundry $adminLaundry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\AdminLaundry  $adminLaundry
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminLaundry $adminLaundry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\AdminLaundry  $adminLaundry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminLaundry $adminLaundry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\AdminLaundry  $adminLaundry
     * @return \Illuminate\Http\Response
     */
}
