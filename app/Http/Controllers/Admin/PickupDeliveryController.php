<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\PickDeliveryBoy;
use App\Http\Controllers\Controller;

class PickupDeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PickDeliveryBoy::all();
        return view('admin-laundry.pickup-boy.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-laundry.pickup-boy.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'hp' => 'required',
            'sex' => 'required',
            'alamat' => 'required',
            'pekerjaan' => 'required',
            'agama' => 'required',
            'tgl_lahir' => 'required',
            'suku' => 'required',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
        ]);

        PickDeliveryBoy::create([
            'nama' => $request->nama,
            'hp' => $request->hp,
            'sex' => $request->sex,
            'pekerjaan' => $request->pekerjaan,
            'agama' => $request->agama,
            'alamat' => $request->alamat,
            'tgl_lahir' => $request->tgl_lahir,
            'suku' => $request->suku,
            'provinsi' => $request->provinsi,
            'kabupaten' => $request->kabupaten,
            'kecamatan' => $request->provinsi,
        ]);
        
        return redirect('admin-laundry/pickup-boy')->with('success', 'Data Berhasi Ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = PickDeliveryBoy::where('id', $id)->first();
        return view('admin-laundry.pickup-boy.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama',
            'hp',
            'sex',
            'alamat',
            'pekerjaan',
            'agama',
            'tgl_lahir',
            'suku',
            'provinsi',
            'kabupaten',
            'kecamatan',
        ]);

        PickDeliveryBoy::where('id', $id)->update([
            'nama' => $request->nama,
            'hp' => $request->hp,
            'sex' => $request->sex,
            'pekerjaan' => $request->pekerjaan,
            'agama' => $request->agama,
            'alamat' => $request->alamat,
            'tgl_lahir' => $request->tgl_lahir,
            'suku' => $request->suku,
            'provinsi' => $request->provinsi,
            'kabupaten' => $request->kabupaten,
            'kecamatan' => $request->provinsi,
        ]);

        return redirect('admin-laundry/pickup-boy')->with('success', 'Data Berhasi Diupdate !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        PickDeliveryBoy::where('id', $id)->delete();
        return redirect('admin-laundry/pickup-boy')->with('warning', 'Data Berhasi Didelete !');
    }
}
