<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PickupBoy extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pickup_boy')->insert([
            'rumah_sakit' => 'Mitra Keluarga',
            'lantai' => '5',
            'ruang_perawatan' => 'Anggrek',
            'pic' => 'Suwardi',
            'status' => 'o',
        ]);
        DB::table('pickup_boy')->insert([
            'rumah_sakit' => 'Mitra Keluarga',
            'lantai' => '6',
            'ruang_perawatan' => 'Melati',
            'pic' => 'Anita',
            'status' => 'o',
        ]);
        DB::table('pickup_boy')->insert([
            'rumah_sakit' => 'Mitra Keluarga',
            'lantai' => '8',
            'ruang_perawatan' => 'Melati',
            'pic' => 'Agus',
            'status' => 'o',
        ]);
        DB::table('pickup_boy')->insert([
            'rumah_sakit' => 'Mayapada',
            'lantai' => '2',
            'ruang_perawatan' => 'Neptunus',
            'pic' => 'Richard',
            'status' => 'o',
        ]);
        DB::table('pickup_boy')->insert([
            'rumah_sakit' => 'Mayapada',
            'lantai' => '3',
            'ruang_perawatan' => 'Pluto',
            'pic' => 'Betrice',
            'status' => 'o',
        ]);
        DB::table('pickup_boy')->insert([
            'rumah_sakit' => 'Mayapada',
            'lantai' => '5',
            'ruang_perawatan' => 'Yupiter',
            'pic' => 'Sonny',
            'status' => 'o',
        ]);
    }
}
