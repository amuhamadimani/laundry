<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_client', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('rumah_sakit')->nullabe();
            $table->integer('lantai')->nullabe();
            $table->integer('ruang_perawatan')->nullabe();
            $table->string('pic')->nullabe();
            $table->string('item_code');
            $table->string('item_name');
            $table->string('qty');
            $table->date('tanggal');
            $table->string('pickup_boy');
            $table->time('jam');
            $table->string('status')->default('s');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_client');
    }
}
