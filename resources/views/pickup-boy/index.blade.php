@extends('templates.stisla')

@section('title', 'Laundry')

@section('header')
	<div class="section-header">
		<h1>Pickup Service - Dashboard</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
		</div>
	</div>
@endsection
    
@section('sidebar-menu')
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li><a class="nav-link" href="{{ url('pickup-boy') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Dashboard"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
    <li class="menu-header">List</li>
    <li><a class="nav-link" href="{{ route('pickup.boy.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Pickup Boy"><i class="fas fa-file"></i><span>Pickup Boy</span></a></li>
    <li><a class="nav-link" href="{{ route('delivery.boy.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Delivery Boy"><i class="fas fa-file"></i><span>Delivery Boy</span></a></li>
  </ul>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-hover" id="dabel">
                  <h4 class="card-title">List Order Open</h4>

                    <thead class="text-center">
                        <tr>
                            <th>No</th>
                            <th>Rumah Sakit</th>
                            <th>Lantai</th>
                            <th>Ruang Perawatan</th>
                            <th>PIC</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $val)
                            <tr>
                              <td>{{ $loop->index+1 }}</td>
                              <td>{{ $val->rumah_sakit }}</td>
                              <td>{{ $val->lantai }}</td>
                              <td>{{ $val->ruang_perawatan }}</td>
                              <td>{{ $val->pic }}</td>
                              <td>
                                  @if ($val->status == 'r')
                                      <div class="badge badge-warning">O</div>
                                  @else
                                      <div class="badge badge-warning">P</div>
                                  @endif
                              </td>
                              <td>
                                <a href="{{ url('pickup-boy/edit/'.$val->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                              </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>   
                
                <div>
                  <hr>
                </div>
                
                <table class="table table-bordered table-hover" id="dabel">
                  <h4 class="card-title">List Order Close</h4>
                  <thead class="text-center">
                      <tr>
                          <th>No</th>
                          <th>Rumah Sakit</th>
                          <th>Lantai</th>
                          <th>Ruang Perawatan</th>
                          <th>PIC</th>
                          <th>Status</th>
                      </tr>
                  </thead>
                  <tbody>
                     @foreach ($dataClose as $val)
                        <tr>
                          <td>{{ $loop->index+1 }}</td>
                              <td>{{ $val->rumah_sakit }}</td>
                              <td>{{ $val->lantai }}</td>
                              <td>{{ $val->ruang_perawatan }}</td>
                              <td>{{ $val->pic }}</td>
                              <td>
                                  @if ($val->status == 'a')
                                      <div class="badge badge-success">C</div>
                                  @endif
                              </td>
                        </tr>
                    @endforeach
                      <tr>
                  </tbody>
              </table>   
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection