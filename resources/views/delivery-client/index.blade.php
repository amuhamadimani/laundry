@extends('templates.stisla')

@section('title', 'Laundry')

@section('header')
	<div class="section-header">
		<h1>Pickup Service - Dashboard</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('pickup-client') }}">Dashboard</a>
			</div>
		</div>
	</div>
@endsection
    
@section('sidebar-menu')
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li><a class="nav-link" href="{{ url('pickup-client') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Dashboard"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
    <li class="menu-header">List</li>
    <li><a class="nav-link" href="{{ route('pickup.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Pickup Client"><i class="fas fa-file"></i><span>Pickup Client</span></a></li>
    <li><a class="nav-link" href="{{ route('pickup.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Delivery Client"><i class="fas fa-file"></i><span>Delivery Client</span></a></li>
  </ul>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
            <a href="{{ route('delivery.client.create') }}" class="btn btn-info btn-lg float-right"><i class="fa fa-plus"></i></a>
                <br>
                <table class="table table-bordered table-striped table-hover" id="dabel">
                  <h4 class="card-title">List Of Order</h4>

                    <thead class="text-center">
                        <tr>
                            <th>No</th>
                            <th>Rumah Sakit</th>
                            <th>Lantai</th>
                            <th>Ruang Perawatan</th>
                            <th>PIC</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($data as $val)
                            <tr>
                              <td>{{ $loop->index+1 }}</td>
                              <td>{{ $val->rumah_sakit }}</td>
                              <td>{{ $val->lantai }}</td>
                              <td>{{ $val->ruang_perawatan }}</td>
                              <td>{{ $val->pic }}</td>
                              <td>
                                @if ($val->status == 's')
                                    <div class="badge badge-warning">S</div>
                                @elseif($val->status == 'r')
                                    <div class="badge badge-info">R</div>
                                  @elseif($val->status == 'd')
                                      <div class="badge badge-info">D</div>
                                @endif
                              </td>
                              <td>
                                <a href="{{ url('delivery-client/edit/'.$val->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                              </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table> 
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection