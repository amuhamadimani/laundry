<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('no_client');
            $table->string('no_kontrak');
            $table->date('tanggal');
            $table->string('client_provinsi');
            $table->string('nama');
            $table->text('alamat');
            $table->mediumInteger('telepon');
            $table->string('pic');
            $table->string('provinsi');
            $table->string('kabupaten');
            $table->string('kecamatan');
            $table->integer('kode_pos');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
