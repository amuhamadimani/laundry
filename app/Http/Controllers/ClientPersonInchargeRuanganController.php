<?php

namespace App\Http\Controllers;

use App\Model\ClientPersonInchargeRuangan;
use Illuminate\Http\Request;

class ClientPersonInchargeRuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\ClientPersonInchargeRuangan  $clientPersonInchargeRuangan
     * @return \Illuminate\Http\Response
     */
    public function show(ClientPersonInchargeRuangan $clientPersonInchargeRuangan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\ClientPersonInchargeRuangan  $clientPersonInchargeRuangan
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientPersonInchargeRuangan $clientPersonInchargeRuangan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\ClientPersonInchargeRuangan  $clientPersonInchargeRuangan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientPersonInchargeRuangan $clientPersonInchargeRuangan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\ClientPersonInchargeRuangan  $clientPersonInchargeRuangan
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientPersonInchargeRuangan $clientPersonInchargeRuangan)
    {
        //
    }
}
