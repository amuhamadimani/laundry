<?php

namespace App\Http\Controllers\PickupDeliveryClient;

use Illuminate\Http\Request;
use App\Model\DeliveryClient;
use App\Model\PickupClient;
use App\Http\Controllers\Controller;

class DeliveryClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DeliveryClient::all();
        return view('delivery-client.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd($request->isMethod('POST'));
        if ($request->isMethod('POST')) 
        {
            // dd($request->isMethod('POST'));
            $request->validate([
                'rumah_sakit' => 'required',
                'lantai' => 'required',
                'ruang_perawatan' => 'required',
                'pic' => 'required',
                'tanggal' => 'required',
                'item_code' => 'required',
                'item_name' => 'required',
                'qty' => 'required',
                'pickup_boy' => 'required',
                'jam' => 'required',
            ]);
            
            DeliveryClient::create([
                'rumah_sakit' => $request->rumah_sakit,
                'lantai' => $request->lantai,
                'ruang_perawatan' => $request->ruang_perawatan,
                'pic' => $request->pic,
                'tanggal' => $request->tanggal,
                'item_code' => json_encode($request->item_code),
                'item_name' => json_encode($request->item_name),
                'qty' => json_encode($request->qty),
                'pickup_boy' => $request->pickup_boy,
                'jam' => $request->jam,
            ]);

            return redirect('delivery-client')->with('success', 'Data Berhasil Ditambahkan');
        }

        return view('delivery-client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $edit = DeliveryClient::where('id', $id)->first();
        $editItem = DeliveryClient::select('item_code', 'item_name', 'qty')->where('id', $id)->get();
        
        if ($request->isMethod('POST')) 
        {
            DeliveryClient::where('id', $id)->update([
                'status' => 'd',
            ]);

            return redirect('delivery-client')->with('success', 'Data Berhasil Diapprove');
        }

        return view('delivery-client.edit', compact('edit', 'editItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
