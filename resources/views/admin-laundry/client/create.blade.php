@php
    $date = date('Y-m-d');    
    $jam = date('H:i:s');    
@endphp

@extends('templates.stisla')

@section('title', 'Laundry')

@section('sidebar-menu')
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li><a class="nav-link" href="{{ url('/admin') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Dashboard"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
    <li class="menu-header">List</li>
    <li><a class="nav-link" href="{{ route('admin.laundry.pickup.boy.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Pickup & Delivery Boy"><i class="fas fa-user"></i><span>Pickup & Delivery Boy</span></a></li>
    <li><a class="nav-link" href="{{ route('admin.laundry.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Maintenance Client"><i class="fas fa-user"></i><span>Maintenance Client</span></a></li>
    <li><a class="nav-link" href="{{ route('admin.laundry.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Maintenance Kontrak"><i class="fas fa-file"></i><span>Maintenance Kontrak</span></a></li>
    <li><a class="nav-link" href="{{ route('users') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Management User"><i class="fas fa-users"></i><span>Management User</span></a></li>
   </ul>
@endsection

@section('header')
	<div class="section-header">
		<h1>Maintenance - Dashboard</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ route('admin.laundry.index') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ route('admin.laundry.client.index') }}">Client</a>
			</div>
			<div class="breadcrumb-item active">
				<a>Create</a>
			</div>
		</div>
	</div>
@endsection
    
@section('content')
<form action="{{ route('admin.laundry.client.store') }}" method="POST">
    @csrf
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
        
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No Client</label>
                        <div class="col-md-7">
                            <input type="text" name="no_client" class="form-control" required="" value="" >
                        </div>
                    </div>    
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No Kontrak</label>
                        <div class="col-md-7">
                            <select name="no_kontrak" id="" class="form-control">
                                @foreach ($kontrak as $val)
                                    <option value="{{ $val->no_kontrak }}">{{ $val->no_kontrak }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>  
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                        <div class="col-md-7">
                            <input type="text" name="nama" class="form-control" required="" value="" >
                        </div>
                    </div>  
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alamat</label>
                        <div class="col-md-7">
                            <textarea name="alamat" class="form-control" id="" cols="30" rows="10"></textarea>
                        </div>
                    </div>   
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Telepon</label>
                        <div class="col-md-7">
                            <input type="number" name="telepon" class="form-control" required="" value="" >
                        </div>
                    </div>  
                        
                </div>
            
                <div class="col-md-6">
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tanggal</label>
                        <div class="col-md-7">
                        <input type="date" name="tanggal" class="form-control" required="" value="{{ $date }}">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">PIC</label>
                        <div class="col-md-7">
                            <input type="text" name="pic" class="form-control" required="" value="" >
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Provinsi</label>
                        <div class="col-md-7">
                            <input type="text" name="provinsi" class="form-control" required="" value="" >
                        </div>
                    </div>
                    
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kabupaten</label>
                        <div class="col-md-7">
                            <input type="text" name="kabupaten" class="form-control" required="" value="" >
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kecamatan</label>
                        <div class="col-md-7">
                            <input type="text" name="kecamatan" class="form-control" required="" value="" >
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kode pos</label>
                        <div class="col-md-7">
                            <input type="number" max="6" name="kode_pos" class="form-control" required="" value="" >
                        </div>
                    </div>
                        <div class="card-footer text-right">
                            <a href="{{ route('admin.laundry.client.index') }}" class="btn btn-danger btn-lg">Batal</a>
                            <button class="btn btn-primary btn-lg">Simpan</button>
                        </div>
                    
                </div>
      </div>
    </div>
</div>
</form>

@endsection

