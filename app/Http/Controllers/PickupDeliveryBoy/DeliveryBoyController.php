<?php

namespace App\Http\Controllers\PickupDeliveryBoy;

use Illuminate\Http\Request;
use App\Model\DeliveryBoy;
use App\Model\DeliveryClient;
use App\Http\Controllers\Controller;

class DeliveryBoyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DeliveryClient::where('status', 's')->get();
        $dataClose = DeliveryClient::where('status', 'r')->get();
        return view('delivery-boy.index', compact('data', 'dataClose'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $edit = DeliveryClient::where('id', $id)->first();
        $editItem = DeliveryClient::select('item_code', 'item_name', 'qty')->where('id', $id)->get();
        
        if ($request->isMethod('POST')) 
        {
            // dd($request->qty);
            DeliveryClient::where('id', $id)->update([
                'status' => 'r',
                'qty' => json_encode($request->qty),
            ]);

            return redirect('delivery-boy')->with('success', 'Data Berhasil Diapprove');
        }

        return view('delivery-boy.edit', compact('edit', 'editItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
