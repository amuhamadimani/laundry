<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClientPersonInchargeRuangan extends Model
{
    protected $table = 'client_person_incharge_ruangan';

    protected $fillable = [
        'client_pic_id',
        'client_pic_name',
        'client_pic_no_telp',
        'client_pic_ph1',
        'client_pic_ph2',
        'client_pic_email',
    ];
}
