<?php

namespace App\Http\Controllers\Admin;

use App\Model\Kontrak;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KontrakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kontrak::all();
        return view('admin-laundry.kontrak.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-laundry.kontrak.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'no_kontrak' => 'required',
            'client' => 'required',
            'periode_kontrak_mulai' => 'required',
            'periode_kontrak_akhir' => 'required|after_or_equal:periode_kontrak_mulai',
            'jenis_kegiatan' => 'required',
            'kuota_minimal' => 'required',
            'berat_minimal' => 'required',
            'periode_tagihan' => 'required',
            'jatuh_tempo' => 'required',
            'terbit_invoice' => 'required',
            'blokir_pickup' => 'required',
            'kode_item' => 'required',
            'nama_item' => 'required',
            'berat' => 'required',
            'harga' => 'required',
        ]);

        Kontrak::create([
            'no_kontrak' => $request->no_kontrak,
            'client' => $request->client,
            'periode_kontrak_mulai' => $request->periode_kontrak_mulai,
            'periode_kontrak_akhir' => $request->periode_kontrak_akhir,
            'jenis_kegiatan' => $request->jenis_kegiatan,
            'kuota_minimal' => $request->kuota_minimal,
            'berat_minimal' => $request->berat_minimal,
            'periode_tagihan' => $request->periode_tagihan,
            'jatuh_tempo' => $request->jatuh_tempo,
            'terbit_invoice' => $request->terbit_invoice,
            'blokir_pickup' => $request->blokir_pickup,
            'kode_item' => json_encode($request->kode_item),
            'nama_item' => json_encode($request->nama_item),
            'berat' => json_encode($request->berat),
            'harga' => json_encode($request->harga),
        ]);

        return redirect('admin-laundry/kontrak/create')->with('success', 'Data Berhasi Ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Kontrak  $kontrak
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Kontrak  $kontrak
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Kontrak::where('id', $id)->first();
        $editItem = Kontrak::select('kode_item', 'nama_item', 'harga', 'berat')->where('id', $id)->get();

        return view('admin-laundry.kontrak.edit', compact('edit','editItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Kontrak  $kontrak
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Kontrak::where('id', $id)->update([
            'no_kontrak' => $request->no_kontrak,
            'client' => $request->client,
            'periode_kontrak_mulai' => $request->periode_kontrak_mulai,
            'periode_kontrak_akhir' => $request->periode_kontrak_akhir,
            'jenis_kegiatan' => $request->jenis_kegiatan,
            'kuota_minimal' => $request->kuota_minimal,
            'berat_minimal' => $request->berat_minimal,
            'periode_tagihan' => $request->periode_tagihan,
            'jatuh_tempo' => $request->jatuh_tempo,
            'terbit_invoice' => $request->terbit_invoice,
            'blokir_pickup' => $request->blokir_pickup,
            'kode_item' => json_encode($request->kode_item),
            'nama_item' => json_encode($request->nama_item),
            'berat' => json_encode($request->berat),
            'harga' => json_encode($request->harga),
        ]);
        return redirect('admin-laundry/kontrak/create')->with('success', 'Data Berhasi Diupdate !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Kontrak  $kontrak
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Kontrak::where('id', $id)->delete();
        return redirect('admin-laundry/kontrak')->with('error', 'Data Berhasi Dihapus !');
    }
}
