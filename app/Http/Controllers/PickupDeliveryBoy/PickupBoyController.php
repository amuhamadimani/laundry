<?php

namespace App\Http\Controllers\PickupDeliveryBoy;

use App\Model\PickupBoy;
use App\Model\PickupClient;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PickupBoyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PickupClient::where('status', 'r')->get();
        $dataClose = PickupClient::where('status', 'a')->get();
        return view('pickup-boy.index', compact('data', 'dataClose'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\PickupBoy  $pickupBoy
     * @return \Illuminate\Http\Response
     */
    public function show(PickupBoy $pickupBoy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\PickupBoy  $pickupBoy
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $edit = PickupClient::where('id', $id)->first();
        $editItem = PickupClient::select('item_code', 'item_name', 'qty')->where('id', $id)->get();
        
        if ($request->isMethod('POST')) 
        {
            // dd($request->qty);
            PickupClient::where('id', $id)->update([
                'status' => 'a',
                'qty' => json_encode($request->qty),
            ]);

            return redirect('pickup-boy')->with('success', 'Data Berhasil Diapprove');
        }

        return view('pickup-boy.edit', compact('edit', 'editItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\PickupBoy  $pickupBoy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PickupBoy $pickupBoy)
    { 
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\PickupBoy  $pickupBoy
     * @return \Illuminate\Http\Response
     */
    public function destroy(PickupBoy $pickupBoy)
    {
        //
    }
}
